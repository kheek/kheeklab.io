module.exports = {
    title: 'Kheek Blog',
    description: 'Vue-powered static site generator running on GitLab Pages',
    base: '/vuepress/',
    dest: 'public'
}
